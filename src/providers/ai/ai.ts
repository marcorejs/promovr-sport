import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuario } from '../../commons/usuario';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Carrera } from '../../commons/carrera';
import { ProfilePage } from '../../pages/profile/profile';

@Injectable()
export class AiProvider {

  constructor(
    public alertCtrl: AlertController,
  ) {

  }

  recomendacionesDisponibles(usuario: Usuario) {
    if (usuario.deporte_fav && usuario.distancia_pref && usuario.eventos_seguidos) {
      return true;
    } else {
      return false;
    }
  }

  alertaAIDisabled() {
    let alert = this.alertCtrl.create({
      title: "Servicio no disponible",
      subTitle: "Porfavor llena tus preferencias en la página de perfil y sigue a algunos eventos",
      buttons: ["OK"]
    });
    alert.present();
  }

  teoremaAlternativoBayes(usuario: Usuario, carrera: Carrera, carreras: Array<Carrera>, ubicacion: string) {
    let resultado = 0;
    const max = usuario.params;
    if (this.recomendacionesDisponibles(usuario)) {
      resultado = resultado + this.eventosSeguidosDep(usuario, carreras, carrera, max.deporte);
      resultado = resultado + this.eventosSeguidosCost(usuario, carreras, carrera, max.costo);
      resultado = resultado + this.eventosSeguidosDist(usuario, carreras, carrera, max.distancia);
      resultado = resultado + this.eventosSeguidosOrg(usuario, carreras, carrera, max.organizador);
      resultado = resultado + this.eventosSeguidosEdo(usuario, carreras, carrera, max.estado, ubicacion);
    }

    return resultado;
  }

  eventosSeguidosDep(usuario: Usuario, carreras: Array<Carrera>, evento: Carrera, peso: number) {
    let result = 0;
    let disciplina = evento.disciplina;
    let ev_seguidos = 0;
    let num_carreras = 0;
    let pce = 0;
    let pc = 0;
    let pe = 0;
    usuario.eventos_seguidos.forEach(ev_seguido => {
      if (ev_seguido.disciplina == disciplina) {
        ev_seguidos++;
      }
    });

    carreras.forEach(carrera => {
      if (carrera.status == "publicada") {
        if (carrera.disciplina == disciplina) {
          num_carreras++;
        }
      }

    })
    pce = ev_seguidos / num_carreras;
    pc = num_carreras / carreras.length;
    pe = usuario.eventos_seguidos.length / carreras.length;
    result = pce * pc;
    result = result / pe;
    result = result * peso;
    console.log("Puntuación deporte " + disciplina + ": " + result);
    return result;
  }

  eventosSeguidosCost(usuario: Usuario, carreras: Array<Carrera>, evento: Carrera, peso: number) {
    let result = 0;
    let costo = evento.costo;
    let ev_seguidos = 0;
    let num_carreras = 0;
    let pce = 0;
    let pc = 0;
    let pe = 0;

    usuario.eventos_seguidos.forEach(ev_seguido => {
      if (ev_seguido.costo <= costo) {
        ev_seguidos++;
      }
    });
    carreras.forEach(carrera => {
      if (carrera.status == "publicada") {
        if (carrera.costo <= costo) {
          num_carreras++;
        }
      }

    });

    pce = ev_seguidos / num_carreras;
    pc = num_carreras / carreras.length;
    pe = usuario.eventos_seguidos.length / carreras.length;

    result = pce * pc;
    result = result / pe;
    result = result * peso;
    console.log("Puntuación costo " + costo + ": " + result);
    return result;
  }

  eventosSeguidosDist(usuario: Usuario, carreras: Array<Carrera>, evento: Carrera, peso: number) {
    let result = 0;
    let distancia = parseInt(evento.distancia);
    let ev_seguidos = 0;
    let num_carreras = 0;
    let pce = 0;
    let pc = 0;
    let pe = 0;

    usuario.eventos_seguidos.forEach(ev_seguido => {
      let evD = parseInt(ev_seguido.distancia);
      let d = distancia;
      if (evD <= (d + 5) || evD >= (d - 5)) {
        ev_seguidos++;
      }
    });
    carreras.forEach(carrera => {
      if (carrera.status == "publicada") {
        let evD = parseInt(carrera.distancia);
        let d = distancia;
        if (evD <= (d + 5) || evD >= (d - 5)) {
          num_carreras++;
        }
      }
    });

    pce = ev_seguidos / num_carreras;
    pc = num_carreras / carreras.length;
    pe = usuario.eventos_seguidos.length / carreras.length;

    result = pce * pc;
    result = result / pe;
    result = result * peso;
    if (result < peso && distancia <= usuario.distancia_pref.max && distancia >= usuario.distancia_pref.min) {
      let aux = result + 1;
      result = (aux <= peso) ? aux : peso;
    }
    console.log("Puntuación distancia " + distancia + ": " + result);
    return result;
  }

  eventosSeguidosOrg(usuario: Usuario, carreras: Array<Carrera>, evento: Carrera, peso: number) {
    let result = 0;
    let promotor = evento.promotor.email;
    let ev_seguidos = 0;
    let num_carreras = 0;
    let pce = 0;
    let pc = 0;
    let pe = 0;

    usuario.eventos_seguidos.forEach(ev_seguido => {
      if (ev_seguido.promotor.email == promotor) {
        ev_seguidos++;
      }
    });

    carreras.forEach(carrera => {
      if (carrera.status == "publicada") {
        if (carrera.promotor.email == promotor) {
          num_carreras++;
        }
      }

    })
    pce = ev_seguidos / num_carreras;
    pc = num_carreras / carreras.length;
    pe = usuario.eventos_seguidos.length / carreras.length;
    result = pce * pc;
    result = result / pe;
    result = result * peso;

    return result;

  }

  eventosSeguidosEdo(usuario: Usuario, carreras: Array<Carrera>, evento: Carrera, peso: number, estado_pref: string) {
    let result = 0;
    let estado = evento.estado;
    let ev_seguidos = 0;
    let num_carreras = 0;
    let pce = 0;
    let pc = 0;
    let pe = 0;

    usuario.eventos_seguidos.forEach(ev_seguido => {
      if (ev_seguido.estado == estado) {
        ev_seguidos++;
      }
    });

    carreras.forEach(carrera => {
      if (carrera.status == "publicada") {
        if (carrera.estado == estado) {
          num_carreras++;
        }
      }

    })
    pce = ev_seguidos / num_carreras;
    pc = num_carreras / carreras.length;
    pe = usuario.eventos_seguidos.length / carreras.length;
    result = pce * pc;
    result = result / pe;
    result = result * (peso * .25);

    if (result < peso && estado == estado_pref) {
      let aux = peso;
      result = (aux <= peso) ? aux : peso;
    }
    console.log("Puntuación estado " + estado + ": " + result);
    return result;
  }

  adjustParameters(usuario: Usuario, parametro: string){
    let result = usuario.params;
    let resta = 0;
    let compensacion = 0;

    switch (parametro) {
      case "deporte":
        resta = result.deporte * 0.2;
        result.deporte = result.deporte - resta;
        compensacion = resta / 4;
        result.costo += parseFloat(compensacion.toFixed(3));
        result.distancia += parseFloat(compensacion.toFixed(3));
        result.estado += parseFloat(compensacion.toFixed(3));
        result.organizador += parseFloat(compensacion.toFixed(3));
        break;
      case "distancia":
        resta = result.distancia * 0.2;
        result.distancia = result.distancia - resta;
        compensacion = resta / 4;
        result.costo += parseFloat(compensacion.toFixed(3));
        result.deporte += parseFloat(compensacion.toFixed(3));
        result.estado += parseFloat(compensacion.toFixed(3));
        result.organizador += parseFloat(compensacion.toFixed(3));
        break;
      case "organizador":
        resta = result.organizador * 0.2;
        result.organizador = result.organizador - resta;
        compensacion = resta / 4;
        result.costo += parseFloat(compensacion.toFixed(3));
        result.deporte += parseFloat(compensacion.toFixed(3));
        result.estado += parseFloat(compensacion.toFixed(3));
        result.distancia += parseFloat(compensacion.toFixed(3));
        break;
      case "estado":
        resta = result.estado * 0.2;
        result.estado = result.estado - resta;
        compensacion = resta / 4;
        result.costo += parseFloat(compensacion.toFixed(3));
        result.deporte += parseFloat(compensacion.toFixed(3));
        result.distancia += parseFloat(compensacion.toFixed(3));
        result.organizador += parseFloat(compensacion.toFixed(3));
        break;
      case "costo":
        resta = result.costo * 0.2;
        result.costo = result.costo - resta;
        compensacion = resta / 4;
        result.distancia += parseFloat(compensacion.toFixed(3));
        result.deporte += parseFloat(compensacion.toFixed(3));
        result.estado += parseFloat(compensacion.toFixed(3));
        result.organizador += parseFloat(compensacion.toFixed(3));

        break;
      default:
        break;
    }
    
    return result;
  }

}
