import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class FuncionesProvider {

  constructor() {
    console.log('Hello FuncionesProvider Provider');
  }

  compareDates(fecha1: string) {
    let dia = new Date().getDate();
    let mes = (new Date().getMonth() + 1);
    let anio = new Date().getFullYear();
    let fecha2 = anio + "-" + mes + "-" + dia;

    let r = fecha1;
    let a1 = fecha1.split("-");
    let a2 = fecha2.split("-");

    let dia1 = parseInt(a1[2]);
    let dia2 = parseInt(a2[2]);

    let mes1 = parseInt(a1[1]);
    let mes2 = parseInt(a2[1]);

    let anio1 = parseInt(a1[0]);
    let anio2 = parseInt(a2[0]);

    if (anio1 > anio2) {
      r = fecha1;
    } else if (anio1 < anio2) {
      r = fecha2;
    } else {
      if (mes1 > mes2) {
        r = fecha1;
      } else if (mes2 > mes1) {
        r = fecha2;
      } else {
        if (dia1 > dia2) {
          r = fecha1;
        } else if (dia2 > dia1) {
          r = fecha2;
        }
      }
    }
    return r;
  }

  eliminarElemArray(array, elem) {
    return array.filter(e => e.nom !== elem.nom && e.tipo !== elem.tipo);
  }

  getYtId(yt_url: string) {
    var video_id = yt_url.split('v=')[1];
    var ampersandPosition = video_id.indexOf('&');
    if (ampersandPosition != -1) {
      video_id = video_id.substring(0, ampersandPosition);
    }
    return video_id
  }

}
