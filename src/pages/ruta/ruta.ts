import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { Carrera } from '../../commons/carrera';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { FuncionesProvider } from '../../providers/funciones/funciones';

declare var google;

@IonicPage()
@Component({
  selector: 'page-ruta',
  templateUrl: 'ruta.html',
})
export class RutaPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  carrera: Carrera;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private database: AngularFirestore,
    private youtube: YoutubeVideoPlayer,
    private func: FuncionesProvider
  ) {
    this.carrera = this.navParams.get('id');
  }

  ionViewDidLoad() {
    this.loadMap();
  }

  loadMap() {
    let latLng = new google.maps.LatLng(this.carrera.ruta[0].lat, this.carrera.ruta[0].lng);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: 'satellite',
      styles: [{
        featureType: 'poi',
        stylers: [{ visibility: 'off' }]
      }, {
        featureType: 'transit.station',
        stylers: [{ visibility: 'off' }]
      }]
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.carrera.ruta.push(this.carrera.ruta[0]);
    var ruta_carrera = new google.maps.Polyline({
      path: this.carrera.ruta,
      geodesic: true,
      strokeColor: '#0000FF',
      strokeOpacity: 1.0,
      strokeWeight: 3
    });

    if (this.carrera.estacionamientos != undefined) {
      this.carrera.estacionamientos.forEach(estacionamiento => {
        var cuadrante_estacionamiento = new google.maps.Polyline({
          path: estacionamiento.puntos,
          geodesic: true,
          strokeColor: "#00FF00",
          strokeOpacity: 1.0,
          strokeWeight: 2
        })
        cuadrante_estacionamiento.setMap(this.map);
      })
    }

    let image_flag = {
      url: "https://firebasestorage.googleapis.com/v0/b/administrador-espacios.appspot.com/o/flag.png?alt=media&token=30498592-bf85-4966-9a60-b078f85447d5",
      size: new google.maps.Size(70, 70),
      origin: new google.maps.Point(7, 0),
      anchor: new google.maps.Point(0, 32),
      scaledSize: new google.maps.Size(40, 40)
    }

    let ruta_length = this.carrera.ruta.length - 1;
    var goal = new google.maps.Marker({
      map: this.map,
      position: new google.maps.LatLng({ lat: this.carrera.ruta[ruta_length].lat, lng: this.carrera.ruta[ruta_length].lng }),
      icon: image_flag
    })

    this.carrera.servicios.forEach(servicio => {
      let latLng = new google.maps.LatLng({ lat: servicio.lat, lng: servicio.lng });
      let image = {
        url: servicio.icon,
        size: new google.maps.Size(70, 70),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32),
        scaledSize: new google.maps.Size(40, 40),
      }
      let marker = new google.maps.Marker({
        map: this.map,
        position: latLng,
        icon: image,
      })
    })
    this.carrera.anunciantes.forEach(anunciante => {
      let latLng = new google.maps.LatLng({ lat: anunciante.lat, lng: anunciante.lng });
      let image = {
        url: anunciante.icon,
        size: new google.maps.Size(60, 43),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32),
        scaledSize: new google.maps.Size(60, 43),
      }
      let marker = new google.maps.Marker({
        map: this.map,
        position: latLng,
        icon: image,
      })

      google.maps.event.addListener(marker, 'click', () => {
        this.verVideo(anunciante.promocional);
      });
    })

    ruta_carrera.setMap(this.map);
  }

  verVideo(promo: string) {
    promo = this.func.getYtId(promo);
    this.youtube.openVideo(promo);
  }

  

}
