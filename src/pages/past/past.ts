import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { Carrera } from '../../commons/carrera';
import { Observable } from 'rxjs/Observable';
import { VerCarreraPage } from '../ver-carrera/ver-carrera';
import { FuncionesProvider } from '../../providers/funciones/funciones';

@Component({
  selector: 'page-past',
  templateUrl: 'past.html'
})
export class PastPage {

  carreras: Observable<Carrera[]>;
  carrerasRef: AngularFirestoreCollection<Carrera[]>;

  constructor(
    public navCtrl: NavController,
    private database: AngularFirestore,
    private funct: FuncionesProvider
  ) {

    this.carrerasRef = this.database.collection<Carrera[]>("carreras", ref => ref.where("status", "==", "publicada"));

    this.carreras = this.carrerasRef.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Carrera;
        const id = a.payload.doc.id;
        return { id, ...data }
      })
    });
  }

  verCarreraPasada(_carrera: Carrera) {
    if(_carrera.status != "pasada") {
      this.database.doc("carreras/" + _carrera.id).update({
        pasada: 1
      })
    }
    this.navCtrl.push(VerCarreraPage, {
      id: _carrera
    })
  }

  compararFechas(fecha1: string) {
    return this.funct.compareDates(fecha1);
  }

}
