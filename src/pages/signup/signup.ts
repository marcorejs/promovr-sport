import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { Usuario } from '../../commons/usuario';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AuthProvider } from '../../providers/auth/auth';
import { Parametros } from '../../commons/parametros';
import { ProfilePage } from '../profile/profile';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  nombre: string;
  email: string;
  password: string;
  r_password: string;
  usuario: Usuario;
  params: Parametros;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private database: AngularFirestore,
    private auth: AuthProvider
  ) {
    this.params = {
      deporte: 6,
      costo: 2,
      estado: 4,
      distancia: 3,
      organizador: 1
    }
  }

  ionViewDidLoad() {

  }

  signUp() {
    if (this.r_password == this.password) {
      this.auth.signup(this.email, this.password).then(r => {
        this.usuario = {
          nombre: this.nombre,
          email: this.email,
          vmn: 12,
          params: this.params
        }
        console.log(this.usuario);
        this.database.collection<Usuario>("users").add(this.usuario);
        this.navCtrl.setRoot(ProfilePage);
      }).catch(err => {
        this.alertCtrl.create({
          title: "Error!",
          subTitle: err.message,
          buttons: ['OK']
        }).present();
      });
      
    } else {
      this.passAlert();
    }
  }

  passAlert() {
    let alert = this.alertCtrl.create({
      title: "Error",
      subTitle: "Las contraseñas no coinciden",
      buttons: ['OK']
    });

    alert.present();
  }
}
