import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Carrera } from '../../commons/carrera';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { FuncionesProvider } from '../../providers/funciones/funciones';

@IonicPage()
@Component({
  selector: 'page-patrocinadores',
  templateUrl: 'patrocinadores.html',
})
export class PatrocinadoresPage {

  carrera: Carrera

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private yt: YoutubeVideoPlayer,
    private func: FuncionesProvider
  ) {
    this.carrera = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatrocinadoresPage');
  }

  verPromo(id: string){
    let promo = this.func.getYtId(id);
    this.yt.openVideo(promo);
  }

}
