import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreRegistroPage } from './pre-registro';

@NgModule({
  declarations: [
    PreRegistroPage,
  ],
  imports: [
    IonicPageModule.forChild(PreRegistroPage),
  ],
})
export class PreRegistroPageModule {}
