import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { Platform } from 'ionic-angular/platform/platform';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Carrera } from '../../commons/carrera';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { AiProvider } from '../../providers/ai/ai';
import { Usuario } from '../../commons/usuario';
import { AuthProvider } from '../../providers/auth/auth';
import { VerCarreraPage } from '../ver-carrera/ver-carrera';
import { ProfilePage } from '../profile/profile';
import * as firebase from 'firebase';
import { FuncionesProvider } from '../../providers/funciones/funciones';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';

@IonicPage()
@Component({
  selector: 'page-recomendaciones',
  templateUrl: 'recomendaciones.html',
})
export class RecomendacionesPage {

  estados = [
    'Aguascalientes',
    'Baja California',
    'Baja California Sur',
    'Campeche',
    'Chiapas',
    'Chihuahua',
    'Coahuila de Zaragoza',
    'Colima',
    'Ciudad de México',
    'Durango',
    'Guanajuato',
    'Guerrero',
    'Hidalgo',
    'Jalisco',
    'Mexico',
    'Michoacan de Ocampo',
    'Morelos',
    'Nayarit',
    'Nuevo Leon',
    'Oaxaca',
    'Puebla',
    'Queretaro de Arteaga',
    'Quintana Roo',
    'San Luis Potosi',
    'Sinaloa',
    'Sonora',
    'Tabasco',
    'Tamaulipas',
    'Tlaxcala',
    'Veracruz-Llave',
    'Yucatan',
    'Zacatecas'
  ];
  estado: string;
  geocorderEnabled: boolean = false;
  carreritas: Array<Carrera> = [];
  carreras: Observable<Carrera[]>;
  carrerasRef: AngularFirestoreCollection<Carrera[]>;
  usuario: Usuario
  carreras_usuario: Array<Carrera> = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private geolocation: Geolocation,
    private geocoder: NativeGeocoder,
    private plat: Platform,
    private database: AngularFirestore,
    private AI: AiProvider,
    private auth: AuthProvider,
    private fun: FuncionesProvider,
    private toastCtrl: ToastController
  ) {
    firebase.auth().onAuthStateChanged(u => {
      if (u) {
        firebase.firestore().collection("users").get().then(snap => {
          snap.forEach(user => {
            if (u.email == user.data().email) {
              this.usuario = user.data() as Usuario;
              this.usuario.id = user.id;
            }
          })
        })
      }
    })
    console.log(this.plat._platforms);
    /*if(this.plat.is('core')){
      this.geocorderEnabled = false;
      this.alertaGeocoderDisabled();
    } else {
      this.geocorderEnabled = true;
      this.geolocation.getCurrentPosition().then(position => {
        console.log(position.coords);
        this.geocoder.reverseGeocode(position.coords.latitude, position.coords.longitude)
          .then((result: NativeGeocoderReverseResult) => {
            this.estado = result.subAdministrativeArea;
            console.log(this.estado);
          });
      });
    }*/

    this.carrerasRef = this.database.collection<Carrera[]>("carreras");

    this.carreras = this.carrerasRef.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Carrera;
        const id = a.payload.doc.id;
        return { id, ...data }
      })
    });

    this.carreras.subscribe(c_list => {
      this.carreritas = c_list.slice();
    })

  }

  ionViewDidLoad() {

  }

  alertaGeocoderDisabled() {
    let alrt = this.alertCtrl.create({
      title: "Servicio de localización desactivado",
      message: "Por favor selecciona tu estado para darte un mejor servicio",
      buttons: ["OK"]
    });
    alrt.present();
  }

  callAi() {
    let r = 0;
    if(this.usuario){
      if(!this.AI.recomendacionesDisponibles(this.usuario)){
        let alerta = this.alertCtrl.create({
          title: "Ayudanos a conocerte",
          message: "Para poder recomendarte eventos por favor sigue algunos eventos de tu interés en el apartado de carreras y completa tus preferencias en tu perfil.",
          buttons: [
            {
              text: "Ir a mi perfil",
              handler: () => {
                this.navCtrl.setRoot(ProfilePage);
              }
            },
            {
              text: "Ahora no"
            }
          ]
        });
        alerta.present();
      } else {
        console.log(this.carreritas);
        this.carreritas.forEach(c => { 
          let can = true;
          if (c.status == "publicada") {
            if(c.no_recomendar){
              c.no_recomendar.forEach(u => {
                if (u == this.usuario.email) {
                  can = false;
                }
              });
            }
            if(c.seguidores){
              c.seguidores.forEach(s => {
                if (s.email == this.usuario.email) {
                  can = false;
                }
              });
            }
            console.log(c);
            r = this.AI.teoremaAlternativoBayes(this.usuario, c, this.carreritas, this.estado);
            console.log(r);
            if (r >= this.usuario.vmn && can) {
              this.carreras_usuario.push(c);
            }
          }
        })
      }
    } else {
      console.log("no hay usuario men");
    }
  }

  verCarrera(_carrera: Carrera) {
    this.navCtrl.push(VerCarreraPage, {
      id: _carrera
    })
  }

  ignoreEvent(_carrera: Carrera){
    let alert = this.alertCtrl.create();
    alert.setTitle('Dinos el motivo');

    alert.addInput({
      type: 'radio',
      label: 'No quiero hacer ese deporte',
      value: 'deporte',
    });
    alert.addInput({
      type: 'radio',
      label: 'Es en otro estado',
      value: 'estado',
    });

    alert.addInput({
      type: 'radio',
      label: 'No suelo hacer esa distancia',
      value: 'distancia'
    });

    alert.addInput({
      type: 'radio',
      label: 'Es caro para mi',
      value: 'costo'
    });

    alert.addInput({
      type: 'radio',
      label: 'No me gusta esa organización',
      value: 'organizador'
    });


    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        let new_params = this.AI.adjustParameters(this.usuario, data);
        let no_recom = (_carrera.no_recomendar) ? _carrera.no_recomendar.slice() : [];
        no_recom.push(this.usuario.email);
        console.log(new_params);
        this.database.doc<Usuario>("users/" + this.usuario.id).update({
          params: new_params
        });
        this.database.doc<Carrera>("carreras/" + _carrera.id).update({
          no_recomendar: no_recom
        });
        this.carreras_usuario = this.fun.eliminarElemArray(this.carreras_usuario, _carrera);
        this.toastAjustar();
      }
    });
    alert.present();
  }

  toastAjustar(){
    let t = this.toastCtrl.create({
      message: "Entendido, ajustamos nuestros parametros para hacer una mejor selección",
      duration: 2000,
      position: "top"
    });
    t.present();
  }

  clearEstado(){
    this.estado = undefined;
  }

}
