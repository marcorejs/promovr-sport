import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { Usuario } from '../../commons/usuario';
import { TabsPage } from '../tabs/tabs';
import { ProfilePage } from '../profile/profile';

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {

  user = { email: "", password: "" };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AuthProvider,
  ) {

  }

  ionViewDidLoad() {

  }

  iniciarSesion() {
    this.auth.login(this.user.email, this.user.password);
    this.navCtrl.setRoot(ProfilePage);
    //subscripcion.unsubscribe();
    
  }

}
