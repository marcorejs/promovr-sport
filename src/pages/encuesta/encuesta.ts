import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Carrera } from '../../commons/carrera';
import { Usuario } from '../../commons/usuario';
import { Encuesta } from '../../commons/encuesta';
import { AngularFirestore } from 'angularfire2/firestore';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { PastPage } from '../past/past';

@IonicPage()
@Component({
  selector: 'page-encuesta',
  templateUrl: 'encuesta.html',
})
export class EncuestaPage {

  opiniones: any;
  general: string;
  ruta: string;
  hidratacion: string;
  carrera: Carrera;
  usuario: Usuario;
  opinion: string;
  opinion_json: Encuesta;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    private database: AngularFirestore
  ) {
    this.opiniones = ['Excelente', 'Buen@', 'Regular', 'Mal@', 'Pesim@'];
    this.carrera = this.navParams.get('id');
    this.usuario = this.navParams.get('usr');
  }

  ionViewDidLoad() {

  }

  sendOpinion() {
    let can = true;
    this.opinion_json = {
      ruta: this.ruta,
      hidratacion: this.hidratacion,
      general: this.general,
      opinion: this.opinion
    }
    if (this.carrera.ya_opinaron) {
      this.carrera.ya_opinaron.forEach(op => {
        if (op.email == this.usuario.email) {
          can = false;
          this.yaOpinoToast();
        }
      });
    }

    if (can) {
      let new_opinions = (this.carrera.encuestas) ? this.carrera.encuestas.slice() : [];
      let usrs = (this.carrera.ya_opinaron) ? this.carrera.ya_opinaron.slice() : [];
      new_opinions.push(this.opinion_json);
      usrs.push(this.usuario);
      this.database.doc("carreras/" + this.carrera.id).update({
        encuestas: new_opinions,
        ya_opinaron: usrs
      })
    }
    this.navCtrl.setRoot(PastPage);
  }

  yaOpinoToast(){
    let t = this.toastCtrl.create({
      message: "Tu ya opinaste previamente sobre este evento",
      duration: 2000,
      position: "top"
    });
    t.present();
  }

}
