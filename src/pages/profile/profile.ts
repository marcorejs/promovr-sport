import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { Usuario } from '../../commons/usuario';
import { NativeStorage } from '@ionic-native/native-storage';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase';
import { LoginPage } from '../login/login';
import { Carrera } from '../../commons/carrera';
import { Observable } from 'rxjs/Observable';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { ConfigPage } from '../config/config';
import { VerCarreraPage } from '../ver-carrera/ver-carrera';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  user: Usuario;
  carreras: Observable<Carrera[]>;
  carrerasRef: AngularFirestoreCollection<Carrera[]>;

  constructor(
    public navCtrl: NavController,
    private auth: AuthProvider,
    private storage: Storage,
    private database: AngularFirestore
  ) {
    firebase.auth().onAuthStateChanged(u => {
      if (u) {
        firebase.firestore().collection("users").get().then(snap => {
          snap.forEach(user => {
            if (u.email == user.data().email) {
              this.user = user.data() as Usuario;
              this.user.id = user.id;
            }
          })
        })
      }
    })

    this.carrerasRef = this.database.collection<Carrera[]>("carreras");

    this.carreras = this.carrerasRef.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Carrera;
        const id = a.payload.doc.id;
        return { id, ...data }
      })
    });

  }

  ionViewDidLoad() {

  }

  salir() {
    this.auth.logout();
    this.navCtrl.setRoot(LoginPage);
  }

  followedEvent(c: Carrera): boolean {
    let r = false;
    if (this.user) {
      if(this.user.eventos_seguidos){
        this.user.eventos_seguidos.forEach(e => {
          if (e.nombre == c.nombre) {
            r = true;
          }
        })
      }
    }
    return r;
  }

  goToConfig(_user:Usuario){
    this.navCtrl.push(ConfigPage, {
      id: _user
    })
  }

  verCarrera(_carrera: Carrera){
    this.navCtrl.push(VerCarreraPage, {
      id: _carrera
    });
  }

}
