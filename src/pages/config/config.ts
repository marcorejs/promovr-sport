import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Usuario } from '../../commons/usuario';
import { AngularFirestore } from 'angularfire2/firestore';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { minmax } from '../../commons/minmax';

@IonicPage()
@Component({
  selector: 'page-config',
  templateUrl: 'config.html',
})
export class ConfigPage {

  usuario: Usuario;
  deportes: Array<string> = ["Carrera pedestre", "Ciclismo", "Ciclismo de montaña", "Caminata"];
  deporte_fav: string;
  distancia: minmax;
  tipo_distancia: string;
  genero: string;
  edad: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    private database: AngularFirestore
  ) {
    this.usuario = this.navParams.get('id');
    this.distancia = { min: 0, max: 0 };
  }

  ionViewDidLoad() {

  }

  saveConfig() {
    let minMax: minmax = {
      min: this.distanceCalculator(this.distancia.min),
      max: this.distanceCalculator(this.distancia.max)
    }
    let preferencias = {
      deporte_fav: this.deporte_fav,
      distancia_pref: minMax,
      genero: this.genero,
      edad: parseInt(this.edad)
    }
    this.database.doc("users/" + this.usuario.id).update(preferencias).then(() => {
      this.toastCtrl.create({
        message: "Cambios guardados con éxito",
        duration: 2000,
        position: 'top'
      }).present();
      this.navCtrl.pop();
    })
  }

  distanceCalculator(dist: number) {
    let result = dist;
    if (this.tipo_distancia == "millas") {
      dist = dist * 1.609;
      result = dist;
    }
    return result;
  }

}
