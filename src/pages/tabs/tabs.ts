import { Component } from '@angular/core';
import { CarrerasPage } from '../carreras/carreras';
import { PastPage } from '../past/past';
import { RecomendacionesPage } from '../recomendaciones/recomendaciones';
import { ProfilePage } from '../profile/profile';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';
import * as firebase from 'firebase';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = CarrerasPage;
  tab2Root = PastPage;
  tab3Root = RecomendacionesPage;
  tab4Root = LoginPage;
  label = "Iniciar Sesión";
  constructor(
    private auth: AuthProvider
  ) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.label = "Mi Perfil";
      } else {
        this.label = "Iniciar Sesión";
      }
    })
  }
}
