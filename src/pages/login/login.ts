import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SigninPage } from '../signin/signin';
import { SignupPage } from '../signup/signup';
import * as firebase from 'firebase';
import { ProfilePage } from '../profile/profile';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.navCtrl.setRoot(ProfilePage);
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goToSignInForm(){
    this.navCtrl.push(SigninPage);
  }
  goToSignUpForm(){
    this.navCtrl.push(SignupPage);
  }

}
