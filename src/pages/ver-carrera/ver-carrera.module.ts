import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerCarreraPage } from './ver-carrera';

@NgModule({
  declarations: [
    VerCarreraPage,
  ],
  imports: [
    IonicPageModule.forChild(VerCarreraPage),
  ],
})
export class VerCarreraPageModule {}
