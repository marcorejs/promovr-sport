import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Carrera } from '../../commons/carrera';
import { RutaPage } from '../ruta/ruta';
import { KitPage } from '../kit/kit';
import { ComunidadPage } from '../comunidad/comunidad';
import { PromocionesPage } from '../promociones/promociones';
import { PreparacionPage } from '../preparacion/preparacion';
import { PatrocinadoresPage } from '../patrocinadores/patrocinadores';
import { PremiosPage } from '../premios/premios';
import { PreRegistroPage } from '../pre-registro/pre-registro';
import { EncuestaPage } from '../encuesta/encuesta';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AuthProvider } from '../../providers/auth/auth';
import { Usuario } from '../../commons/usuario';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { FuncionesProvider } from '../../providers/funciones/funciones';

@IonicPage()
@Component({
  selector: 'page-ver-carrera',
  templateUrl: 'ver-carrera.html',
})
export class VerCarreraPage {

  carrera: Carrera;
  distancia_millas: string;
  public segment_var = "menus";
  menus: Array<any>;
  usuario: Usuario;
  contador_seguidores: number;
  followed: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public browser: InAppBrowser,
    private auth: AuthProvider,
    private alertCtrl: AlertController,
    private database: AngularFirestore,
    private f: FuncionesProvider
  ) {
    this.carrera = this.navParams.get('id');
    this.distancia_millas = (parseInt(this.carrera.distancia) / 1.609).toFixed(0) + " millas";
    this.usuario = this.auth.usuario;
    this.followed = this.alreadyFollowed();
    this.menus = [
      { title: "Ruta", icon: "map", component: RutaPage, disponible: 1 },
      { title: "Kit de la carrera", icon: "document", component: KitPage, disponible: 1 },
      { title: "Comunidad", icon: "contacts", component: ComunidadPage, disponible: 1 },
      { title: "Promociones", icon: "cart", component: PromocionesPage, disponible: 1 },
      { title: "Preparación física", icon: "body", component: PreparacionPage, disponible: 1 },
      { title: "Patrocinadores", icon: "clipboard", component: PatrocinadoresPage, disponible: 1 },
      { title: "Premios", icon: "trophy", component: PremiosPage, disponible: 1 },
      //{ title: "Pre-registro", icon: "card", component: PreRegistroPage, disponible: 1 },
      { title: "Encuesta", icon: "help", component: EncuestaPage, disponible: this.encuestaDisponible() },
    ]

    this.contador_seguidores = (this.carrera.seguidores) ? this.carrera.seguidores.length : 0;
  }

  ionViewDidLoad() {

  }

  openMenu(menu) {
    this.navCtrl.push(menu.component, {
      id: this.carrera,
      usr: this.usuario
    });
  }

  realidadPage() {
    this.browser.create("https://marcoredmg.github.io/avideo", '_system', {
      allowInlineMediaPlayback: 'yes'
    })
  }

  followEvent() {
    var followers_event = (this.carrera.seguidores) ? this.carrera.seguidores.slice() : [];
    followers_event.push(this.usuario);
    var followed_events = (this.usuario.eventos_seguidos) ? this.usuario.eventos_seguidos.slice() : [];
    followed_events.push(this.carrera);


    this.database.doc("carreras/" + this.carrera.id).update({
      seguidores: followers_event
    });
    this.database.doc("users/" + this.usuario.id).update({
      eventos_seguidos: followed_events
    });

    this.followed = true;
    this.contador_seguidores++;
    this.followAlert();
  }

  unfollowEvent() {
    let index_u;
    let index_c;
    let followers = this.carrera.seguidores;
    let followed = this.usuario.eventos_seguidos;

    for (let i = 0; i < this.usuario.eventos_seguidos.length; i++) {
      if (this.usuario.eventos_seguidos[i].nombre == this.carrera.nombre) {
        index_u = i;
        break;
      }
    }
    for (let i = 0; i < this.carrera.seguidores.length; i++) {
      if (this.carrera.seguidores[i].email == this.usuario.email) {
        index_c = i;
        break;
      }
    }

    followed.splice(index_u);
    followers.splice(index_c);
    console.log(followed);
    console.log(followers);

    this.database.doc("carreras/" + this.carrera.id).update({
      seguidores: followers
    });

    this.database.doc("users/" + this.usuario.id).update({
      eventos_seguidos: followed
    });

    this.followed = false;
    this.contador_seguidores--;
    this.unfollowAlert();
  }

  alreadyFollowed(): boolean {
    let r = false;
    if (this.carrera.seguidores) {
      this.carrera.seguidores.forEach(seg => {
        if (seg.email == this.usuario.email) {
          r = true;
        }
      });
    }
    return r;
  }

  followAlert() {
    let alerta = this.alertCtrl.create({
      title: "Sigues a este evento",
      subTitle: "Ahora sigues a este evento",
      buttons: ["OK"]
    });
    alerta.present();
  }

  unfollowAlert() {
    let alerta = this.alertCtrl.create({
      title: "Ya no sigues a este evento",
      subTitle: "Dejaste de seguir a este evento",
      buttons: ["OK"]
    });
    alerta.present();
  }

  unfollowConfirm() {
    let alerta = this.alertCtrl.create({
      title: "¿Dejar de seguir?",
      subTitle: "¿Quieres dejar de seguir al evento?",
      buttons: [
        {
          text: 'Dejar de seguir',
          handler: () => {
            this.unfollowEvent();
          }
        },
        {
          text: 'Regresar',
          handler: () => {

          }
        },
      ]
    });
    alerta.present();
  }

  encuestaDisponible(): number {
    let r = 0;
    if (this.carrera.pasada == 1 || this.f.compareDates(this.carrera.fecha) != this.carrera.fecha) {
      r = 1;
    }
    return r;
  }


}
