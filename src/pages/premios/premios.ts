import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Carrera } from '../../commons/carrera';

@IonicPage()
@Component({
  selector: 'page-premios',
  templateUrl: 'premios.html',
})
export class PremiosPage {

  carrera: Carrera;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.carrera = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PremiosPage');
  }

}
