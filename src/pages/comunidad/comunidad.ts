import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Carrera } from '../../commons/carrera';

@IonicPage()
@Component({
  selector: 'page-comunidad',
  templateUrl: 'comunidad.html',
})
export class ComunidadPage {

  carrera: Carrera;
  imagenes: Array<any>;
  memes: Array<any>;
  public segment_var = 'galeria';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.carrera = this.navParams.get('id');
    this.memes = this.carrera.memes;
    this.imagenes = this.carrera.galeria;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComunidadPage');
  }

}
