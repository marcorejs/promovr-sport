import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreparacionPage } from './preparacion';

@NgModule({
  declarations: [
    PreparacionPage,
  ],
  imports: [
    IonicPageModule.forChild(PreparacionPage),
  ],
})
export class PreparacionPageModule {}
