import { Carrera } from "./carrera";
import { minmax } from "./minmax";
import { Parametros } from "./parametros";

export interface Usuario {
    nombre: string,
    email: string,
    vmn: number,
    deporte_fav?: string,
    distancia_pref?: minmax,
    prom_costo?: number,
    eventos_seguidos?: Array<Carrera>,
    id?: string,
    genero?: string,
    params: Parametros,
    edad?: number
}