import { Usuario } from "./usuario";
import { Promotor } from "./promotor";
import { Encuesta } from "./encuesta";


export interface Carrera {
    nombre: string,
    ruta: Array<any>,
    servicios: Array<any>,
    costo: string,
    anunciantes: Array<any>,
    distancia: string,
    estacionamientos?: Array<any>,
    kit?: any,
    memes?: Array<any>,
    galeria?: Array<any>,
    seguidores?: Array<Usuario>,
    id?: string,
    disciplina: string,
    fecha: string,
    estado: string,
    status: string,
    promotor: Promotor,
    no_recomendar?: Array<string>,
    encuestas?: Array<Encuesta>,
    ya_opinaron?: Array<Usuario>,
    pasada: number
}