export interface Parametros {
    deporte: number,
    costo: number,
    estado: number,
    organizador: number,
    distancia: number
}