export interface Promotor {
    nombre: string,
    email: string,
    tipo: string
}