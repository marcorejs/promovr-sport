export interface Encuesta {
    general: string,
    ruta: string,
    hidratacion: string,
    opinion: string
}