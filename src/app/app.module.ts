import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

//Modules
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule, AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { NativeStorage } from '@ionic-native/native-storage';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';

//Providers & Services
import { AiProvider } from '../providers/ai/ai';
import { AuthProvider } from '../providers/auth/auth';

//Pages
import { ProfilePage } from '../pages/profile/profile';
import { PastPage } from '../pages/past/past';
import { CarrerasPage } from '../pages/carreras/carreras';
import { TabsPage } from '../pages/tabs/tabs';
import { VerCarreraPage } from '../pages/ver-carrera/ver-carrera';
import { RutaPage } from '../pages/ruta/ruta';
import { KitPage } from '../pages/kit/kit';
import { ComunidadPage } from '../pages/comunidad/comunidad';
import { RecomendacionesPage } from '../pages/recomendaciones/recomendaciones';
import { PreparacionPage } from '../pages/preparacion/preparacion';
import { PromocionesPage } from '../pages/promociones/promociones';
import { PremiosPage } from '../pages/premios/premios';
import { PatrocinadoresPage } from '../pages/patrocinadores/patrocinadores';
import { PreRegistroPage } from '../pages/pre-registro/pre-registro';
import { EncuestaPage } from '../pages/encuesta/encuesta';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { SigninPage } from '../pages/signin/signin';
import { ConfigPage } from "../pages/config/config";
import { FuncionesProvider } from '../providers/funciones/funciones';


export const FIREBASE_CONFIG = {
  apiKey: "AIzaSyCQk8dT3Px1moCZ6tDfGDC1zsNTO1q3dVk",
  authDomain: "administrador-espacios.firebaseapp.com",
  databaseURL: "https://administrador-espacios.firebaseio.com",
  projectId: "administrador-espacios",
  storageBucket: "administrador-espacios.appspot.com",
  messagingSenderId: "250008479103"
};

@NgModule({
  declarations: [
    MyApp,
    ProfilePage,
    PastPage,
    CarrerasPage,
    TabsPage,
    RecomendacionesPage,
    VerCarreraPage,
    RutaPage,
    KitPage,
    ComunidadPage,
    PreparacionPage,
    PromocionesPage,
    PremiosPage,
    PatrocinadoresPage,
    PreRegistroPage,
    EncuestaPage,
    LoginPage,
    SignupPage,
    SigninPage,
    ConfigPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFirestoreModule,
    IonicImageViewerModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProfilePage,
    PastPage,
    CarrerasPage,
    TabsPage,
    RecomendacionesPage,
    VerCarreraPage,
    RutaPage,
    KitPage,
    ComunidadPage,
    PreparacionPage,
    PromocionesPage,
    PremiosPage,
    PatrocinadoresPage,
    PreRegistroPage,
    EncuestaPage,
    LoginPage,
    SignupPage,
    SigninPage,
    ConfigPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AiProvider,
    AuthProvider,
    YoutubeVideoPlayer,
    InAppBrowser,
    NativeStorage,
    Geolocation,
    NativeGeocoder,
    FuncionesProvider
  ]
})
export class AppModule {}
